﻿using System.Collections.Generic;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTOs;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IVehicleServiceApi
    {
        ICollection<Vehicle> GetVehicles();
        Vehicle GetVehicle(string vehicleId);
        void AddVehicle(VehicleDTO vehicle);
        void DeleteVehicle(string vehicleId);
    }
}
