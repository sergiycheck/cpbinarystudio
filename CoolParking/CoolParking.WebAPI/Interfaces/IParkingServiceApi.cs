﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IParkingServiceApi
    {
        decimal GetBalance();
        int GetCapacity();
        int GetFreePlaces();
    }
}
