﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTOs;

namespace CoolParking.WebAPI.Interfaces
{
    public interface ITransactionsServiceApi
    {
        TransactionInfo[] GetLastParkingTransactions();
        string GetAllTransactions();
        Vehicle TopUpVehicle(TransactInfoDto infoDto);

    }
}
