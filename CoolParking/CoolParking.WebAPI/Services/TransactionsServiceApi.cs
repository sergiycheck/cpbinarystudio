﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTOs;
using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Services
{
    public class TransactionsServiceApi: ITransactionsServiceApi
    {
        private readonly BaseService _service;

        public TransactionsServiceApi()
        {
            _service = BaseService.GetInstance();
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _service.ParkingService.GetLastParkingTransactions();
        }

        public string GetAllTransactions()
        {
            return _service.ParkingService.ReadFromLog();
        }

        public Vehicle TopUpVehicle(TransactInfoDto infoDto)
        {
            if (!_service.Validator.Validate(infoDto.Id))
            {
                throw new InvalidOperationException();
            }
            var vehicle = _service.ParkingService.GetVehicles().FirstOrDefault(v => v.Id == infoDto.Id);
            if (vehicle == null)
            {
                throw new ArgumentException();
            }
            _service.ParkingService.TopUpVehicle(infoDto.Id, infoDto.Sum);
            return _service.ParkingService.GetVehicles().FirstOrDefault(v => v.Id == infoDto.Id);
        }
    }
}
