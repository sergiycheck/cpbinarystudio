﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Helpers;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTOs;
using CoolParking.WebAPI.Interfaces;
using Microsoft.AspNetCore.Server.IIS;

namespace CoolParking.WebAPI.Services
{
    public class VehicleServiceApi: IVehicleServiceApi
    {
        private readonly BaseService _service;
        public VehicleServiceApi()
        {
            _service = BaseService.GetInstance();
        }

        public ICollection<Vehicle> GetVehicles()
        {
            return _service.ParkingService.GetVehicles();
        }

        public Vehicle GetVehicle(string vehicleId)
        {
            if (!_service.Validator.Validate(vehicleId))
            {
                throw new InvalidOperationException();
            }
            var vehicle = _service.ParkingService.GetVehicles().FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException();
            }
            return vehicle;
        }

        public void AddVehicle(VehicleDTO vehicleDto)
        {
            var type = (VehicleType)vehicleDto.VehicleType;
            Vehicle vehicle = new Vehicle(vehicleDto.Id, type, vehicleDto.Balance);
            _service.ParkingService.AddVehicle(vehicle);
        }

        public void DeleteVehicle(string vehicleId)
        {
            if (!_service.Validator.Validate(vehicleId))
            {
                throw new InvalidOperationException();
            }
            var vehicle = _service.ParkingService.GetVehicles().FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException();
            }
            _service.ParkingService.RemoveVehicle(vehicleId);
        }

    }
}
