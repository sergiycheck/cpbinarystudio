﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoolParking.BL.Helpers;
using CoolParking.BL.Services;

namespace CoolParking.WebAPI.Services
{
    public class BaseService
    {
        public readonly ParkingService ParkingService;
        public readonly HttpClient Client;
        public readonly IdManager Validator;
        private static readonly object Locker = new Object();
        private static BaseService _instance;
        protected BaseService()
        {
            ParkingService = new ParkingService(new TimerService(), new TimerService(), new LogService());
            Client = new HttpClient();
            Validator = new IdManager();
        }
        public static BaseService GetInstance()
        {
            Console.WriteLine($"GetInstance {DateTime.Now.TimeOfDay}");
            if (_instance == null)
            {
                lock (Locker)
                {
                    if (_instance == null)
                        _instance = new BaseService();
                }
            }
            return _instance;
        }
    }
}
