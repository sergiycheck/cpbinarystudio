﻿using System.Net.Http;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Services
{
    public class ParkingServiceAPI: IParkingServiceApi
    {
        private readonly BaseService _service;

        public ParkingServiceAPI()
        {
            _service = BaseService.GetInstance();
        }

        public decimal GetBalance()
        {
            return _service.ParkingService.GetBalance();
        }
        public int GetCapacity()
        {
            return _service.ParkingService.GetCapacity();
        }
        public int GetFreePlaces()
        {
            return _service.ParkingService.GetFreePlaces();
        }
        

    }
}
