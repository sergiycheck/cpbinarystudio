﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.DTOs
{
    public class VehicleDTO
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("vehicleType")]
        public int VehicleType { get; set; }
        [JsonProperty("balance")]
        public decimal Balance { get; set; }

    }
}
