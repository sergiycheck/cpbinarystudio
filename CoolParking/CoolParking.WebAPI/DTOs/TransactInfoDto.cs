﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CoolParking.WebAPI.DTOs
{
    public struct TransactInfoDto
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
    }
}
