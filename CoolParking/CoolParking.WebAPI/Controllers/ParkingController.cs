﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingServiceApi _parkingServiceAPI;
        
        public ParkingController(IParkingServiceApi service)
        {
            _parkingServiceAPI = service;
            
        }
        [HttpGet]
        [Route("api/parking/balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(_parkingServiceAPI.GetBalance());
        }

        [HttpGet]
        [Route("api/parking/capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(_parkingServiceAPI.GetCapacity());
        }

        [HttpGet]
        [Route("api/parking/freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parkingServiceAPI.GetFreePlaces());
        }

    }
}
