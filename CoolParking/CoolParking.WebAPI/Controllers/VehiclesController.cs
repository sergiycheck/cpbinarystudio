﻿using System;
using System.Collections.Generic;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTOs;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IVehicleServiceApi _vehicleServiceApi;

        public VehiclesController(IVehicleServiceApi service)
        {
            _vehicleServiceApi = service;
        }
        
        [HttpGet]
        public ActionResult<ICollection<Vehicle>> GetVehicles()
        {
            return Ok(_vehicleServiceApi.GetVehicles());
        }

        
        [HttpGet("{id}")]
        public ActionResult<string> GetVehicle(string id)
        {
            try
            {
                var vehicle =_vehicleServiceApi.GetVehicle(id);
                return Ok(vehicle);
            }
            catch (Exception e)
            {
                if (e is InvalidOperationException)
                {
                    return BadRequest();
                }

                if (e is ArgumentException)
                {
                    return NotFound();
                }
            }
            return NoContent();
        }

        
        [HttpPost]
        public ActionResult<Vehicle> PostVehicles([FromBody] VehicleDTO vehicle)
        {
            try
            {
                _vehicleServiceApi.AddVehicle(vehicle);
                return Created(nameof(GetVehicle),vehicle);
            }
            catch (Exception e)
            {
                return BadRequest();
            }

        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(string id)
        {
            try
            {
                _vehicleServiceApi.DeleteVehicle(id);
            }
            catch (Exception e)
            {
                if (e is InvalidOperationException)
                {
                    return BadRequest();
                }

                if (e is ArgumentException)
                {
                    return NotFound();
                }
            }

            return NoContent();
        }
    }
}
