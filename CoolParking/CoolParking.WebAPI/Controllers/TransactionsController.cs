﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTOs;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly ITransactionsServiceApi _transactionsServiceApi;

        public TransactionsController(ITransactionsServiceApi service)
        {
            _transactionsServiceApi = service;
        }
        
        [HttpGet]
        [Route("api/transactions/last")]
        public ActionResult<TransactionInfo[]> GetLastTransactions()
        {
            return Ok(_transactionsServiceApi.GetLastParkingTransactions());
        }

        [HttpGet]
        [Route("api/transactions/all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                return Ok(_transactionsServiceApi.GetAllTransactions());
            }
            catch (Exception e)
            {
                return NotFound();
            }
        }

        [HttpPut]
        [Route("api/transactions/topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle([FromBody] TransactInfoDto infoDto)
        {
            try
            {
                return Ok(_transactionsServiceApi.TopUpVehicle(infoDto));
            }
            catch (Exception e)
            {
                if (e is InvalidOperationException)
                {
                    return BadRequest();
                }

                if (e is ArgumentException)
                {
                    return NotFound();
                }
            }

            return NoContent();
        }

    }
}
