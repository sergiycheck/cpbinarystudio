﻿
using System;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking
{
    class Program
    {
        static void Menu()
        {
            var vehicle = new Vehicle("AA-1332-BB",VehicleType.PassengerCar,20);

            var withdrawTimer = new TimerService();
            var logTimer = new TimerService();

            ParkingService service = new ParkingService(withdrawTimer, logTimer, new LogService());

            int selection = 0;

            Console.WriteLine("Press 1 Display the current balance of the Parking.");
            Console.WriteLine("Press 2 Display the amount of earned funds for the current period (before recording in the log).");
            Console.WriteLine("Press 3 Display the number of free parking spaces (free X with Y).");
            Console.WriteLine("Press 4 Display all Parking Transactions for the current period (before logging).");
            Console.WriteLine("Press 5 Display the transaction history (reading data from the Transactions.log file).");
            Console.WriteLine("Press 6 Display a list of Tr. funds in the parking lot.");
            Console.WriteLine("Press 7 Put the vehicle in the parking lot.");
            Console.WriteLine("Press 8 Remove the Vehicle from the Parking.");
            Console.WriteLine("Press 9 Replenish the balance of a particular Tr. means.");

            Console.WriteLine("10 - Exit");

            while (selection != 10)
            {
                try
                {
                    string act = Console.ReadLine();
                    selection = int.Parse(act);
                }
                catch (Exception e)
                {

                }


                switch (selection)
                {
                    case 1:
                        Console.WriteLine($"Balance : {service.GetBalance()}");
                        break;
                    case 2:
                        Console.WriteLine($"Earned money : {service.GetBalance()}");
                        break;
                    case 3:
                        Console.WriteLine($"Fre {service.GetFreePlaces()} places from {service.GetCapacity()}");
                        break;
                    case 4:
                        foreach (var transaction in service.GetLastParkingTransactions())
                        {
                            Console.WriteLine($"Transactions : {transaction.ToString()}");
                        }
                        
                        break;
                    case 5:
                        Console.WriteLine($"Transactions history : {service.ReadFromLog()}");
                        break;
                    case 6:
                        foreach (var car in service.GetVehicles())
                        {
                            Console.WriteLine($"Id : {car.Id}");
                        }
                        break;
                    case 7:
                        service.AddVehicle(vehicle);
                        Console.WriteLine("Vehicle has been successfully added!");
                        break;
                    case 8:
                        service.RemoveVehicle(vehicle.Id);
                        Console.WriteLine("Vehicle has been successfully removed!");
                        break;
                    case 9:
                        try
                        {
                            Console.WriteLine("Enter sum");
                            var sum = int.Parse(Console.ReadLine());
                            service.TopUpVehicle(vehicle.Id,sum);
                            Console.WriteLine("Vehicle balance has been successfully replenished!");
                        }
                        catch (Exception e)
                        {

                        }
                        break;
                }


            }

            Console.WriteLine(service.GetBalance());
            service.Dispose();

        }
        static void Main(string[] args)
        {
            Menu();
        }
    }
}
