﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; private set; }
        public DateTime Time { get; private set; }
        public string CarId { get; private set; }

        public TransactionInfo(decimal sum, DateTime time, string carId)
        {
            Sum = sum;
            Time = time;
            CarId = carId;
        }

        public override string ToString()
        {
            return string.Format($"sum: {Sum}, time: {Time}, Car id: {CarId}");
        }
    }

}
