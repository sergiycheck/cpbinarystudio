﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation,
//       which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly
//       generated unique identifier.

using System;
using System.Text;
using CoolParking.BL.Helpers;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        IdManager manager = new IdManager();
        private  string _id;

        public string Id
        {
            get { return _id; }
            private set
            {
                if (manager.Validate(value))
                {
                    _id = value;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }

        static string GenerateRandomRegistrationPlateNumber()
        {
            return IdManager.GenerateRandomRegistrationPlateNumber();

        }
        public  VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }

        public Vehicle()
        {

        }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {

            Id = id;
            VehicleType = vehicleType;
            if(balance>0)
                Balance = balance;
            else
                throw new ArgumentException();


        }
    }

}
