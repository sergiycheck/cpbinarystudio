﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal Balance = 0;
        public static int Volume = 10;
        public static int WithDrawMoneyPeriod = 5000;//Change here 5000
        public static int LoggingPeriod = 60000;//change here 60000
        public static decimal CarTariff = 2;
        public static decimal TruckTariff = 5;
        public static decimal BusTariff = Convert.ToDecimal(3.5);
        public static decimal MotorcycleTariff = 1;
        public static decimal FineCoefficient = Convert.ToDecimal(2.5);

    }

}