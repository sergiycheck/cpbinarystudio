﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static object _locker = new Object();
        private static Parking _instance;
        public decimal Balance { get; set; }

        public List<Vehicle> Vehicles
        {
            get => _vehicles;
            set => _vehicles = value;
        }
        private  List<Vehicle> _vehicles;
        private readonly ReadOnlyCollection<Vehicle> _vehiclesReadOnlyCollection;
        protected Parking()
        {
            _vehicles = new List<Vehicle>(10);
            _vehiclesReadOnlyCollection = new ReadOnlyCollection<Vehicle>(_vehicles);
        }
        public int GetCapacity() => _vehicles.Capacity;
        public int GetFreePlaces() => _vehicles.Capacity - _vehicles.Count;

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _vehiclesReadOnlyCollection;
        }


        public static Parking GetInstance()
        {
            Console.WriteLine($"GetInstance {DateTime.Now.TimeOfDay}");
            if (_instance == null)
            {
                lock (_locker)
                {
                    if (_instance == null)
                        _instance = new Parking();
                }
            }
            return _instance;
        }
    }

}