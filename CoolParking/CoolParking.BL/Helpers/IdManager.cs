﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Helpers
{
    public class IdManager
    {
        public bool Validate(string CarNum)
        {
            string phrase = CarNum;
            string[] words = { };
            try
            {
                words = phrase.Split('-');
                if (words.Length > 3)
                {
                    Console.WriteLine("All Length is not correct");
                    return false;
                }

                if (words[0].Length != 2)
                {
                    Console.WriteLine("Length 0 is not correct");
                    return false;
                }
                else
                {
                    foreach (var n in words[0])
                    {
                        if (!Char.IsLetter(n))
                        {
                            Console.WriteLine("not a letter inside 0");
                            return false;
                        }

                        if (!Char.IsUpper(n))
                        {
                            Console.WriteLine("Char is not in upper case 0");
                            return false;
                        }

                    }
                }


                if (words[1].Length != 4)
                {
                    Console.WriteLine("Length 1 is not correct");
                    return false;
                }
                else
                {
                    foreach (var n in words[1])
                    {
                        if (!Char.IsDigit(n))
                        {
                            Console.WriteLine("not a numbers inside 1");
                            return false;
                        }

                    }
                }

                if (words[2].Length != 2)
                {
                    Console.WriteLine("Length 2 is not correct");
                }
                else
                {
                    foreach (var n in words[2])
                    {
                        if (!Char.IsLetter(n))
                        {
                            Console.WriteLine("not a letter inside 2");
                            return false;
                        }

                        if (!Char.IsUpper(n))
                        {
                            Console.WriteLine("Char is not in upper case 2");
                            return false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exeption:{e}, Not a valid format");
                return false;
            }

            foreach (var word in words)
            {
                System.Console.WriteLine($"{word}");
            }

            return true;
        }

        private static StringBuilder GetChars(Random rnd)
        {
            char ch;
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < 2; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * rnd.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random rnd = new Random();
            string middle = "";
            for (int i = 0; i < 4; i++)
            {
                middle += rnd.Next(0, 9).ToString();
            }
            StringBuilder builder = GetChars(rnd);

            string part1 = builder.ToString().ToUpper();

            StringBuilder builder1 = GetChars(rnd);

            string part2 = builder1.ToString().ToUpper();
            return string.Format($"{part1}-{middle}-{part2}");
        }
    }
}
