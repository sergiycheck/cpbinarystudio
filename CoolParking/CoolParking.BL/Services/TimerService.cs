﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using System.Timers;
using CoolParking.BL.Interfaces;


namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {

        public double Interval
        {
            get => _timer.Interval;
            set => _timer.Interval = value; 
        }
        private readonly Timer _timer;
        public event ElapsedEventHandler Elapsed;

        //we can extract timer event and call it from class that encapsulates this event and timer
        public TimerService()
        {
            _timer = new Timer();
        }

        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this, null);
        }
        public void SetElapsed(ElapsedEventHandler func)
        {
            _timer.Elapsed += func;
        }
        public void Dispose()
        {
            _timer.Dispose();
        }

        public void Start()
        {
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Stop();
        }
    }

}