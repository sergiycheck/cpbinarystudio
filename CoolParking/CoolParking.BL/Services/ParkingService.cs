﻿// TODO: implement the ParkingService class from the IParkingService interface.
//For try to add a vehicle on full parking InvalidOperationException should be thrown.
//For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//Other validation rules and constructor format went from tests.
//Other implementation details are up to you, they just have to match the interface requirements
//and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;

        private readonly ILogService _logService;
        private readonly Parking _parking;
        private readonly List<TransactionInfo> _transactionInfos;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            _parking = Parking.GetInstance();
            _transactionInfos = new List<TransactionInfo>();
            SetWithDrawTimer();
            SetLogTimer();
        }

        public void WithDrawMoney(object source, ElapsedEventArgs e)
        {
            TransactionInfo info;
            decimal sum = 0;

            if (_parking.Vehicles.Count > 0)
            {
                Console.WriteLine("Logging add vehicles from list to log...");
                foreach (var vehicle in _parking.Vehicles)
                {
                    if (vehicle.VehicleType == VehicleType.Bus)
                    {
                        if (vehicle.Balance > 0)
                        {
                            //3<5
                            if (vehicle.Balance < Settings.BusTariff)
                            {
                                sum = Convert.ToDecimal(vehicle.Balance + (Settings.BusTariff - vehicle.Balance) * Settings.FineCoefficient);
                                vehicle.Balance -= sum;
                            }
                            else
                            {
                                sum = Settings.BusTariff;
                                vehicle.Balance -= sum;
                            }

                        }
                        else
                        {
                            //balance <0
                            sum = Settings.BusTariff * Settings.FineCoefficient;
                            vehicle.Balance -= sum;
                        }

                    }
                    if (vehicle.VehicleType == VehicleType.Motorcycle)
                    {
                        if (vehicle.Balance > 0)
                        {
                            //0.5<1
                            if (vehicle.Balance < Settings.MotorcycleTariff)
                            {
                                sum = Convert.ToDecimal(vehicle.Balance + (Settings.MotorcycleTariff - vehicle.Balance) * Settings.FineCoefficient);
                                vehicle.Balance -= sum;
                            }
                            else
                            {
                                sum = Settings.MotorcycleTariff;
                                vehicle.Balance -= sum;

                            }
                        }
                        else
                        {
                            //balance <0
                            sum = Settings.MotorcycleTariff * Settings.FineCoefficient;
                            vehicle.Balance -= sum;
                        }
                    }
                    if (vehicle.VehicleType == VehicleType.PassengerCar)
                    {
                        if (vehicle.Balance > 0)
                        {
                            //1<2
                            if (vehicle.Balance < Settings.CarTariff)
                            {
                                sum = Convert.ToDecimal(vehicle.Balance + (Settings.CarTariff - vehicle.Balance) * Settings.FineCoefficient);
                                vehicle.Balance -= sum;
                            }
                            else
                            {
                                sum = Settings.CarTariff;
                                vehicle.Balance -= sum;
                            }
                        }
                        else
                        {
                            //balance <0
                            sum = Settings.CarTariff * Settings.FineCoefficient;
                            vehicle.Balance -= sum;
                        }
                    }
                    if (vehicle.VehicleType == VehicleType.Truck)
                    {
                        if (vehicle.Balance > 0)
                        {
                            //2<3.5
                            if (vehicle.Balance < Settings.TruckTariff)
                            {
                                sum = Convert.ToDecimal(vehicle.Balance + (Settings.TruckTariff - vehicle.Balance) * Settings.FineCoefficient);
                                vehicle.Balance -= sum;
                            }
                            else
                            {
                                sum = Settings.TruckTariff;
                                vehicle.Balance -= sum;
                            }
                        }
                        else
                        {
                            //balance <0
                            sum = Settings.TruckTariff * Settings.FineCoefficient;
                            vehicle.Balance -= sum;
                        }
                    }

                    info = new TransactionInfo(sum, DateTime.Now, vehicle.Id);
                    _parking.Balance += sum;
                    _transactionInfos.Add(info);

                }
            }

        }

        public void SetLogTimer()
        {
            _logTimer.Elapsed += WriteToLog;
            _logTimer.SetElapsed(WriteToLog);
            _logTimer.Interval = Settings.LoggingPeriod;
            _logTimer.Start();
        }
        public void SetWithDrawTimer()
        {

            _withdrawTimer.Elapsed += WithDrawMoney;
            _withdrawTimer.SetElapsed(WithDrawMoney);
            _withdrawTimer.Interval = Settings.WithDrawMoneyPeriod;
            _withdrawTimer.Start();
        }
        //can not add vehicle with same id
        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.Vehicles.Count == _parking.Vehicles.Capacity)
            {
                throw new System.InvalidOperationException();
            }
            else
            {
                if (_parking.Vehicles.Count > 0)
                {
                    if (_parking.Vehicles.Exists(v => v.Id == vehicle.Id)) 
                        throw new ArgumentException();
                    else
                        _parking.Vehicles.Add(vehicle);
                }
                else
                {
                    _parking.Vehicles.Add(vehicle);
                }
                
            }
                
        }

        private void StopTimers()
        {
            _logTimer.Stop();
            _withdrawTimer.Stop();
        }
        public void Dispose()
        {
            StopTimers();
            _logTimer.Dispose();
            _withdrawTimer.Dispose();
            _parking.Vehicles.Clear();
        }

        public decimal GetBalance() => _parking.Balance;

        public int GetCapacity() => _parking.GetCapacity();

        public int GetFreePlaces()=>_parking.GetFreePlaces();
        

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactionInfos.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return _parking.GetVehicles();
        }

        public void WriteToLog(object source, ElapsedEventArgs e)
        {
            if (_transactionInfos.Count == 0)
                return;
            Console.WriteLine("Writing log to file...");
            foreach (var transacts in _transactionInfos)
            {
                _logService.Write(transacts.ToString());
            }
            _transactionInfos.Clear();
            _parking.Balance = 0;// success

        }
        public string ReadFromLog()
        {
           return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicleToRemove = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

            if (vehicleToRemove?.Balance < 0)
            {
                throw new System.InvalidOperationException();
            }
            else
            {
                if(!_parking.Vehicles.Exists(v=>v.Id==vehicleId))
                    throw new ArgumentException();
                else
                    _parking.Vehicles.Remove(vehicleToRemove);
            }
            
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (!_parking.Vehicles.Exists(v => v.Id == vehicleId))
                throw new ArgumentException();

            var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle != null)
            {
                if (sum > 0)
                    vehicle.Balance += Convert.ToDecimal(sum);
                else
                    throw new ArgumentException();
                
            }
                
        }
    }

}