﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System.IO;
using System.Text;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath =>
            //@"E:\studying\binary studio\github page\CoolParking\bsa20-dotnet-hw2-template\CoolParking\CoolParking.BL.Tests\bin\Debug\netcoreapp3.1\Transactions.log";
            @"E:\studying\binary studio\github page\CoolParking\Logs\Transactions.log";

        public string Read()
        {
            using (StreamReader sr = new StreamReader(LogPath))
            {
                return sr.ReadToEnd();
            }
        }

        public LogService(string path)
        {
            
        }
        public LogService()
        {

        }


        public void Write(string logInfo)
        {
            using (StreamWriter sw = new StreamWriter(LogPath, true, Encoding.Default))
            {
                sw.WriteLine(logInfo);
            }
        }
    }

}